const express = require('express');
const router = express.Router();
const http = require("http");
const path = require("path");
const logFilePath = path.join(__dirname, "../request.log");
const uuID = require("crypto");


router.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "../homePage.html"));
  });


  router.get("/html", (req, res) => {
    res.sendFile(path.join(__dirname, "../data.html"));
  });


  router.get("/json", (req, res) => {
    res.sendFile(path.join(__dirname, "../data.json"));
  });


  router.get("/uuid", (req, res) => {
    const uuIDV4 = uuID.randomUUID();

    res.send(`uuid: ${uuIDV4}`);
  });


  router.get("/status/:STATUS_CODE", (req, res) => {

    if (req.params.STATUS_CODE in http.STATUS_CODES) {
      res.status(Number(req.params.STATUS_CODE));

      res.send(
        `${req.params.STATUS_CODE}: ${
          http.STATUS_CODES[req.params.STATUS_CODE]
        }`
      );

    } else {
      res.status(400);
      res.send(`${400}:Bad Request`);
    }
    
  });

  router.get("/delay/:Delay_time", (req, res) => {
    if (
      typeof req.params.Delay_time !== "number" &&
      req.params.Delay_time >= 0
    ) {
      setTimeout(() => {
        res.send(`response sent after : ${req.params.Delay_time} seconds`);
      }, req.params.Delay_time * 1000);

    } else {
      res.status(400);
      res.send(`Given time is not valid`);
      
    }
  });

  router.get("/logs", (req, res) => {

    res.sendFile(logFilePath);

  });


  module.exports = router;