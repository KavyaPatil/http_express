const requestId = require("express-request-id");
const express = require("express");
const { port } = require("./config");
const fs = require("fs");
const http = require("http");
const path = require("path");
const router = require("./routers/getRouter");
const logRequestMiddleware = require("./middleware/logRequest");
const errorHandlerMiddleware = require("./middleware/errorHandler");
const unexpectedRequestHandler = require("./middleware/unexpectedRequestHandler");

const app = express();

function workingWIthHttpUsingExpress() {
  
  app.use(requestId());

  app.use(logRequestMiddleware); //logging the logRequestMiddleware

  app.use(router);

  app.use(unexpectedRequestHandler);

  app.use(errorHandlerMiddleware);

  app.listen(port, () => {
    console.log("on Live");
  });
}

workingWIthHttpUsingExpress();
